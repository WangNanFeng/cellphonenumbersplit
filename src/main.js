import Vue from 'vue'
import App from './App.vue'
import './css/index.css'
import VueParticles from 'vue-particles'
// 粒子特效
Vue.use(VueParticles)

new Vue({
  el: '#app',
  render: h => h(App)
})
