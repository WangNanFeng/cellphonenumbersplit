/**
 * Created by hc on 2016/11/25.
 */
export const PHONE_YYS_NAME_MAP = {
  "130": "联通",
  "131": "联通",
  "132": "联通",
  "133": "电信",
  "134": "移动",
  "135": "移动",
  "136": "移动",
  "137": "移动",
  "138": "移动",
  "139": "移动",
  "145": "联通",
  "147": "移动",
  "150": "移动",
  "151": "移动",
  "152": "移动",
  "153": "电信",
  "155": "联通",
  "156": "联通",
  "157": "移动",
  "158": "移动",
  "159": "移动",
  "1700": "电信",
  "1701": "电信",
  "1703": "移动",
  "1705": "移动",
  "1706": "移动",
  "1707": "联通",
  "1708": "联通",
  "1709": "联通",
  "171": "联通",
  "173": "电信",
  "175": "联通",
  "176": "联通",
  "177": "电信",
  "178": "移动",
  "180": "电信",
  "181": "电信",
  "182": "移动",
  "183": "移动",
  "184": "移动",
  "185": "联通",
  "186": "联通",
  "187": "移动",
  "188": "移动",
  "189": "电信"
};
